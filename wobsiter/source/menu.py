# 
# Copyright 2008 Benedikt Sauer
# 
# This file is part of Wobsiter.
#
# Wobsiter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Wobsiter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Wobsiter. If not, see <http://www.gnu.org/licenses/>.
# 
from ..util import change_ext

from xml.dom import minidom

class MenuHandler(object):
    def __init__(self, path):
        self._path = path

    def build(self, output):
        root_node = minidom.parse(self._path).firstChild

        self.title = root_node.getAttribute("title")

        self.items = [
                (node.firstChild.nodeValue,
                 change_ext(node.getAttribute("source"), 'html'))
                for node in root_node.getElementsByTagName("page")
                ]

        return self

